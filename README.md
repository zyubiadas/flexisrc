# flexisrc

create a new project on your localhost

git init

git remote add origin https://zyubiadas@bitbucket.org/zyubiadas/flexisrc.git

git pull origin master



composer install/update



Go to .env file and update as needed

Open your workbench and create a schema named "flexisrc" 

php artisan migrate

*go back to your workbench and check if tables "list" and "list_members" are present



####################################################################
Since I am on a Windows desktop, I tested this RESTful API using BASH.Listed below are the URLS ready to be tested just in case:
<br/>Please also note that I am using Laragon hence the .dev extension on the URL. 
Change as needed depending on your localhost setup




cURL



CREATE A LIST

curl http://flexisrc.dev/list/create \
  -d '{"name": "My First Post!"}' \
  -H 'Content-Type: application/json'

UPDATE A LIST -- change the value of X with the listID you want to update

curl http://flexisrc.dev/list/x/update \
  -d '{"name": "My First Postnow upted!"}' \
  -H 'Content-Type: application/json'

DELETE A LIST -- change the value of X with the listID you want to delete

curl http://flexisrc.dev/list/X/delete \
  -d '' \
  -H 'Content-Type: application/json'

ADD A MEMBER TO A LIST -- change the value of X with the listID, value of Y with the memberID

curl http://flexisrc.dev/list/X/add_member/Y \
  -d '' \
  -H 'Content-Type: application/json'

DELETE A MEMBER ON A LIST -- change the value of X with the listID, value of Y with the memberID

curl http://flexisrc.dev/list/X/delete_member/Y \
  -d '' \
  -H 'Content-Type: application/json'