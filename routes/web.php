<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});


$router->group(['prefix' => 'list'], function () use ($router) {
    //CREATE A LIST
    $router->post('/create', function (\Illuminate\Http\Request $request)    {
        if ($request->isJson()) {
            $data = $request->json()->all();
        } else {
            $data = $request->all();
        }

        if (isset($data)) {
            if (isset($data['name']) && !empty(trim($data['name']))) {
                $list = \App\Lists::where('name', $data['name'])->first();
                if (!isset($list)) {
                    $list = new \App\Lists();
                    $list->name = $data['name'];
                    $list->save();

                    return json_encode(array('message' => 'list successfully inserted - #'.$list->id, 'id' => $list->id));
                } else {
                    return json_encode(array('message' => 'list name must be unique'));
                }
            } else {
                return json_encode(array('message' => 'error -  name is required'));
            }
        } else {
            return json_encode(array('message' => 'something went wrong'));
        }
    });

    //UPDATE A LIST
    $router->post('/{id}/update', function (\Illuminate\Http\Request $request)    {
        if ($request->isJson()) {
            $data = $request->json()->all();
        } else {
            $data = $request->all();
        }

        if(isset($request->route()[2]['id'])) {
            $id = $request->route()[2]['id'];
        } else {
            return json_encode(array('message' => 'error - id is null'));
        }

        if (isset($data)) {
            $list = \App\Lists::find($id);
            if (isset($list)) {
                \Illuminate\Support\Facades\DB::table('lists')
                    ->where('id', $id)
                    ->update(['name' => $data['name']]);

                return json_encode(array('message' => 'list successfully updated'));
            } else {
                return json_encode(array('message' => 'list not found'));
            }
        } else {
            return json_encode(array('message' => 'something went wrong'));
        }
    });

    //DELETE A LIST
    $router->post('/{id}/delete', function (\Illuminate\Http\Request $request)    {
        if(isset($request->route()[2]['id'])) {
            $id = $request->route()[2]['id'];
            $list = \App\Lists::find($id);
            if (isset($list)) {
                $list->delete();
                return json_encode(array('message' => 'list #'.$id.' successfully deleted'));
            } else {
                return json_encode(array('message' => 'list not found'));
            }
        } else {
            return json_encode(array('message' => 'error - id is null'));
        }
    });

    //ADD MEMBERS WITHIN A LIST
    $router->post('/{list_id}/add_member/{member_id}', function (\Illuminate\Http\Request $request)    {
        if(isset($request->route()[2]['member_id']) && isset($request->route()[2]['list_id'])) {
            $member_id = $request->route()[2]['member_id'];
            $list_id = $request->route()[2]['list_id'];

            $listMember = \App\ListMembers::where('member_id', $member_id)->where('list_id', $list_id)->first();
            if (!isset($listMember)) {
                //check if list exists
                $list = \App\Lists::find($list_id);
                if (isset($list)) {
                    $listMember = new \App\ListMembers();
                    $listMember->list_id = $list_id;
                    $listMember->member_id = $member_id;
                    $listMember->save();
                    return json_encode(array('message' => 'member #'.$member_id.' successfully added to list #'.$list_id));
                } else {
                    return json_encode(array('message' => 'invalid list number!'));
                }
            } else {
                return json_encode(array('message' => 'member #'.$member_id.' already exists on list #'.$list_id));
            }
        } else {
            return json_encode(array('message' => 'error - member_id and list_id must both be present'));
        }
    });

    //DELETE MEMBER WITHIN A LIST
    $router->post('/{list_id}/delete_member/{member_id}', function (\Illuminate\Http\Request $request)    {
        if(isset($request->route()[2]['member_id']) && isset($request->route()[2]['list_id'])) {
            $list_id = $request->route()[2]['list_id'];
            $member_id = $request->route()[2]['member_id'];

            $listMember = \App\ListMembers::where('member_id', $member_id)->where('list_id', $list_id)->first();
            if (isset($listMember)) {
                $listMember = \App\ListMembers::find($listMember->id);
                $listMember->delete();

                return json_encode(array('message' => 'member #'.$member_id.' successfully deleted on list #'.$list_id));
            } else {
                return json_encode(array('message' => 'member #'.$member_id.' does not exists on list #'.$list_id));
            }
        } else {
            return json_encode(array('message' => 'error - member_id and list_id must both be present'));
        }
    });
});