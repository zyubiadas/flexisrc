<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ListMembers extends Model
{
    protected $table = 'list_members';
}
